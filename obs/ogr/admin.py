from django.contrib import admin
from .models import *


admin.site.register(Fakulte)
admin.site.register(Bolum)
admin.site.register(Ogrenci)
admin.site.register(Personel)
admin.site.register(OgretimElemani)
admin.site.register(Donem)
admin.site.register(Ders)
admin.site.register(AcilanDersler)
admin.site.register(Transkript)
admin.site.register(AlinanDersler)
admin.site.register(Notlar)
admin.site.register(Danisman)
admin.site.register(Ogretimyili)
