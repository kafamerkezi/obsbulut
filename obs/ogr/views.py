from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.shortcuts import render_to_response
from django.contrib.auth.forms import *  # formlar
from django.contrib.auth import *  # Login - Logout
from django.contrib.auth.decorators import *  # görünüm gizlenme,izin sorgulama
from django.http import *  # Http komut


def index(request):
    # Giriş yapan bidaha giremesin
    oku = request.user.id
    if oku:
        #Yetkisiz girisleri kesmek icin kullanıcı kontrolü
        if Kullanici.objects.values("personel").filter(auth__username=request.user.username).first().get("personel"):
            if Kullanici.objects.values("is_staff").filter(auth__username=request.user.username).first().get("is_staff"):
                return HttpResponseRedirect('/instructor/')
            else:
                return HttpResponseRedirect('/staff/')
        else:
            return HttpResponseRedirect('/student/')
    # formu çağırdık.
    form = AuthenticationForm
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        giris_kontrol = AuthenticationForm(data=request.POST)
        if giris_kontrol.is_valid():
            kullanici = authenticate(username=username, password=password)
            login(request, kullanici)
            print(Kullanici.objects.values("is_staff").filter(auth__username=username).first().get("unvan"))
            if Kullanici.objects.values("personel").filter(auth__username=username).first().get("personel"):
                if Kullanici.objects.values("is_staff").filter(auth__username=username).first().get("is_staff"):
                    return HttpResponseRedirect('/instructor/')
                else:
                    return HttpResponseRedirect('/staff/')
            else:
                return HttpResponseRedirect('/student/')
    return render(request, 'ogr/index.html', locals())




@login_required(login_url='/')
@permission_required('ogr.sekreter',raise_exception=True)
def staff(request):
    personel = Personel.objects.filter(tc=request.user.username)
    ogryili=Ogretimyili.objects.all().order_by('-id')[0]
    context = {'personel': personel, 'ogryili': ogryili}
    return render(request, 'ogr/staff.html', context)

@login_required(login_url='/')
@permission_required('ogr.sekreter',raise_exception=True)
def course_entry(request):
    if request.method == "POST":
        form = DersForm(request.POST)
        context = {'form': form}
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('/staff/course_entry', pk=post.pk)
    else:
        form = DersForm()
        context = {'form': form}
    return render(request, 'ogr/course_entry.html', context)

@login_required(login_url='/')
@permission_required('ogr.sekreter',raise_exception=True)
def course_open(request):
    if request.method == "POST":
        form = DersAcmaForm(request.POST)
        context = {'form': form}
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('/staff/course_open', pk=post.pk)
    else:
        form = DersAcmaForm()
        context = {'form': form}
    return render(request, 'ogr/course_open.html', context)



@login_required(login_url='/')
@permission_required('ogr.hoca',raise_exception=True)
def instructor(request):
    personel = Personel.objects.filter(tc=request.user.username)
    context = {'personel': personel}
    return render(request, 'ogr/instructor.html', context)

@login_required(login_url='/')
@permission_required('ogr.hoca',raise_exception=True)
def note_entry(request):
    return render_to_response('ogr/note_entry.html')

@login_required(login_url='/')
@permission_required('ogr.hoca',raise_exception=True)
def mentor(request):
    ogryili = Ogretimyili.objects.all().order_by('-id')[0]
    ogrenciler = Ogrenci.objects.all().filter(id__in=Danisman.objects.filter(ogretimElemani__tc=request.user.username).filter(ogretimyili__adi=ogryili.adi).values('ogrenciId__id'))
    context = {'ogrenciler': ogrenciler}
    return render(request, 'ogr/mentor.html', context)

@login_required(login_url='/')
@permission_required('ogr.hoca',raise_exception=True)
def mentor_approval(request):
    ogryili = Ogretimyili.objects.all().order_by('-id')[0]
    alinandersler = AlinanDersler.objects.filter(ogrenci_id__in = Ogrenci.objects.filter(id__in=Danisman.objects.filter(ogretimElemani__tc=request.user.username).filter(ogretimyili__adi=ogryili.adi).values('ogrenciId__id'))).filter(onay=False)
    ogrenciler = Ogrenci.objects.filter(id__in=Danisman.objects.filter(ogretimElemani__tc=request.user.username).filter(ogretimyili__adi=ogryili.adi).values('ogrenciId__id'))
    if request.method == "POST":
        form = DersOnayForm(request.POST,request=request)
        context = {'form': form, 'alinandersler':alinandersler,'ogrenciler':ogrenciler}
        if form.is_valid():
            post = form.save(commit=False)
            post.onay = True
            post.save()
            return redirect('/instructor/mentor/approval', pk=post.pk)
    else:
        form = DersOnayForm(request=request)
        context = {'form': form, 'alinandersler':alinandersler,'ogrenciler':ogrenciler}
    return render(request, 'ogr/mentor_approval.html', context)

@login_required(login_url='/')
@permission_required('ogr.ogrenci',raise_exception=True)
def student(request):
    ogrenci = Ogrenci.objects.filter(tc=request.user.username)
    ogryili=Ogretimyili.objects.all().order_by('-id')[0]
    danisman = Danisman.objects.filter(ogrenciId__tc=request.user.username).filter(ogretimyili=ogryili)[0]  # objects.all().filter(tc=request.user.username)
    context = {'ogrenci': ogrenci, 'danisman': danisman}
    return render(request, 'ogr/student.html', context)

@login_required(login_url='/')
@permission_required('ogr.ogrenci',raise_exception=True)
def exam_results(request):
    return render_to_response('ogr/exam_results.html')

@login_required(login_url='/')
@permission_required('ogr.ogrenci',raise_exception=True)
def transcript(request):
    transkript = Transkript.objects.filter(ogrenci__tc=request.user.username)
    kredi = Ders.objects.all()
    harfnotlari = transkript.values_list('harfnotu', flat=True)  # Harf Notuna göre liste oluşturuyor
    krediler_kr = kredi.values_list('kredi', flat=True)  # Hangi dersin kaç kredi olduğunu listeliyor
    context = {'transkript': transkript, 'ortalama': ortalamaHesapla(harfnotlari, krediler_kr)}
    return render(request, 'ogr/transkript.html', context)

@login_required(login_url='/')
@permission_required('ogr.ogrenci',raise_exception=True)
def course_pick(request):
    if request.method == 'POST':
        print("merhaba")

    else:
        # Ogrenci dönemi çekme
        ogrenci= Ogrenci.objects.filter(tc=request.user.username)
        ogrencidonem=ogrenci.values_list('donem', flat=True)
        donem=Donem.objects.filter(id=ogrencidonem[0])
        donem=donem.values_list('adi', flat=True)
        #doneme gore dersleri gönderme
        acilandersler = AcilanDersler.objects.filter(donem__adi=donem[0])
        context={'acilandersler' : acilandersler}
        return render_to_response('ogr/course_pick.html',context)


@login_required(login_url='/')
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required(login_url='/')
def gettranskript(request):
    transkript = Transkript.objects.filter(ogrenci__tc=request.user.username)
    kredi = Ders.objects.all()
    harfnotlari = transkript.values_list('harfnotu', flat=True)  # Harf Notuna göre liste oluşturuyor
    krediler_kr = kredi.values_list('kredi', flat=True)  # Hangi dersin kaç kredi olduğunu listeliyor
    context = {'transkript': transkript, 'ortalama': ortalamaHesapla(harfnotlari, krediler_kr)}
    return render(request, 'ogr/transkript.html', context)

def ortalamaHesapla(harfnotlari, krediler):
    hesaplama = {'AA': 4, 'BA': 3.5, 'BB': 3, 'CB': 2.5, 'CC': 2, 'DC': 1.5, 'DD': 1, 'FD': 0.5, 'FF': 0}
    ortalama, j = 0, 0
    krediTop = 0
    for i in harfnotlari:
        ortalama += hesaplama[i] * krediler[j]
        krediTop += krediler[j]
        j += 1
    return ortalama / krediTop



