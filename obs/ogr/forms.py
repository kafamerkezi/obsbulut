from django.contrib.auth.models import User
from django import forms
from .models import *




class UserForm(forms.ModelForm):
    password = forms.CharField(min_length=8,widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class DersForm(forms.ModelForm):

    class Meta:
        model = Ders
        fields = ('adi', 'kredi', 'akts' , 'zorunlu')

    def __init__(self, *args, **kwargs):
          super(DersForm, self).__init__(*args, **kwargs)
          for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })



class DersAcmaForm(forms.ModelForm):
    class Meta:
        model = AcilanDersler
        fields = ('ders', 'donem', 'ogretimGorevlisi' , 'kod' , 'ogretimyili')

    def __init__(self, *args, **kwargs):
        super(DersAcmaForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class KayitForm(forms.ModelForm):

    class Meta:
        model = AlinanDersler
        fields = ('ogrenci', 'acilanders', 'onay')

    def __init__(self, *args, **kwargs):
        super(KayitForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class DersOnayForm(forms.ModelForm):
    class Meta:
        model = AlinanDersler
        fields = ('ogrenci', 'acilanders', 'onay')
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(DersOnayForm, self).__init__(*args, **kwargs)
        ogryili = Ogretimyili.objects.all().order_by('-id')[0]
        self.fields["ogrenci"].queryset = Ogrenci.objects.filter(id__in=Danisman.objects.filter(ogretimElemani__tc=self.request.user.username).filter(ogretimyili__adi=ogryili.adi).values('ogrenciId__id'))
        self.fields["acilanders"].queryset = AlinanDersler.objects.filter(ogrenci_id__in = Ogrenci.objects.filter(id__in=Danisman.objects.filter(ogretimElemani__tc=self.request.user.username).filter(ogretimyili__adi=ogryili.adi).values('ogrenciId__id'))).filter(onay=False)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
