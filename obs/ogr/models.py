from django.db import models
from django.contrib.auth import models as usermodels

fakulte_to_str_dict = {
    "Mühendislik": "Mühendisliği",
    "İşletme": "İşletmeciliği"
}

class Ogretimyili(models.Model):
    adi=models.CharField(max_length=15, null=False)

    def __str__(self):
        return self.adi

    class Meta:
        verbose_name_plural = "Öğretim Yılları"


class Fakulte(models.Model):
    isim = models.CharField(max_length=40, null=False)

    def __str__(self):
        return self.isim

    class Meta:
        verbose_name_plural = "Fakülteler"


class Bolum(models.Model):
    isim = models.CharField(max_length=40, null=False)
    fakulte = models.ForeignKey(Fakulte, on_delete=models.CASCADE)

    def __str__(self):
        return self.isim + ' ' + fakulte_to_str_dict.get(str(self.fakulte), "Bölümü")

    class Meta:
        verbose_name_plural = "Bölümler"


class Kullanici(models.Model):
    auth = models.ForeignKey(usermodels.User, on_delete=models.CASCADE)
    is_staff = models.BooleanField(default=False, null=False)


class Donem(models.Model):
    adi = models.CharField(max_length=1, null=False)
    bolum = models.ForeignKey(Bolum, on_delete=models.CASCADE)

    def __str__(self):
        return self.bolum.__str__() + ' - ' + self.adi + '. Dönem'

    class Meta:
        verbose_name_plural = "Dönem"

class Ogrenci(Kullanici):
    adi = models.CharField(max_length=40, null=False)
    soyadi = models.CharField(max_length=25, null=False)
    tc = models.CharField(max_length=11, null=False)
    ogrencino = models.BigIntegerField(null=False)
    kayitTarihi = models.DateField()
    bolum = models.ForeignKey(Bolum, on_delete=models.CASCADE)
    telefon = models.CharField(max_length=10)
    email = models.EmailField()
    donem = models.ForeignKey(Donem, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.ogrencino) + ' ' + str(self.adi) + ' ' + str(self.soyadi)

    class Meta:
        permissions = (("ogrenci","Ogrencidir"),)
        verbose_name_plural = "Öğrenciler"


class Personel(Kullanici):
    is_staff = True

    adi = models.CharField(max_length=40, null=False)
    soyadi = models.CharField(max_length=25, null=False)
    tc = models.CharField(max_length=11, null=False)
    telefon = models.CharField(max_length=10)
    email = models.EmailField()

    def __str__(self):
        return str(self.tc) + ' ' + str(self.adi) + ' ' + str(self.soyadi)

    class Meta:
        permissions = (("sekreter","Personeldir"),)
        verbose_name_plural = "Personeller"


class OgretimElemani(Personel):
    unvan = models.CharField(max_length=20, null=False)
    bolum = models.ForeignKey(Bolum, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.tc) + ' ' + str(self.unvan) + ' ' + str(self.adi) + ' ' + str(self.soyadi)

    class Meta:
        permissions = (("hoca","Öğretim Elemanıdır"),)
        verbose_name_plural = "Öğretim Elemanları"


class Ders(models.Model):
    adi = models.CharField(max_length=40, null=False)
    kredi = models.IntegerField(null=False)
    akts = models.IntegerField(null=False)
    zorunlu = models.BooleanField(default=False,null=False)

    def __str__(self):
        return self.adi + ' ' + str(self.kredi)

    class Meta:
        verbose_name_plural = "Dersler"


class AcilanDersler(models.Model):
    ders = models.ForeignKey(Ders, on_delete=models.CASCADE)
    donem = models.ForeignKey(Donem, on_delete=models.CASCADE)
    ogretimGorevlisi = models.ForeignKey(OgretimElemani, on_delete=models.CASCADE)
    kod = models.CharField(max_length=10, null=False)
#    tarih = models.DateField()
    ogretimyili = models.ForeignKey(Ogretimyili, on_delete=models.CASCADE)

    def __str__(self):
        return self.kod + ' ' + self.ders.__str__() + ' ' + self.donem.__str__()

    class Meta:
        verbose_name_plural = "Açılan Dersler"


class Transkript(models.Model):
    ogrenci = models.ForeignKey(Ogrenci, on_delete=models.CASCADE)
    acilanders = models.ForeignKey(AcilanDersler, on_delete=models.CASCADE)
    harfnotu = models.CharField(max_length=2)

    def __str__(self):
        return self.ogrenci.__str__() + ' - ' + self.acilanders.__str__() + ' - ' + self.harfnotu.__str__()

    class Meta:
        verbose_name_plural = "Transkript"


class AlinanDersler(models.Model):
    ogrenci = models.ForeignKey(Ogrenci, on_delete=models.CASCADE)
    acilanders = models.ForeignKey(AcilanDersler, on_delete=models.CASCADE)
    onay = models.BooleanField(default=False,null=False)

    def __str__(self):
        return self.acilanders.__str__() + ' -> ' + self.ogrenci.__str__()

    class Meta:
        verbose_name_plural = "Alınan Dersler"


class Notlar(models.Model):
    alinanders = models.ForeignKey(AlinanDersler, on_delete=models.CASCADE)
    vize = models.IntegerField()
    final = models.IntegerField()

    def __str__(self):
        return self.alinanders.__str__() + ' -- vize=' + self.vize.__str__() + ' final=' + self.final.__str__()

    class Meta:
        verbose_name_plural = "Notlar"


class Danisman(models.Model):
    ogretimyili = models.ForeignKey(Ogretimyili, on_delete=models.CASCADE)
    ogrenciId = models.ForeignKey(Ogrenci, on_delete=models.CASCADE)
    ogretimElemani = models.ForeignKey(OgretimElemani, on_delete=models.CASCADE)

    def __str__(self):
        return self.ogretimyili.__str__() + ' -> ' + self.ogretimElemani.__str__() + ' - ' + self.ogrenciId.__str__()

    class Meta:
        verbose_name_plural = "Danışmanlar"
