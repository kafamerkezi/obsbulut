from django.conf.urls import url
from . import views
from django.views.generic import RedirectView

urlpatterns = [

	#Ana sayfa urlsi
    url(r'^$', views.index, name='index'),
	#Ogrenci isleri urlleri
	url(r'^staff/$', views.staff, name='staff'),
    url(r'^staff/course_entry/$', views.course_entry, name='course_entry'),
    url(r'^staff/course_open/$', views.course_open, name='course_open'),
	#Ogretmen Urlleri
    url(r'^instructor/$', views.instructor, name='instructor'),
    url(r'^instructor/note_entry/$', views.note_entry, name='note_entry'),
    url(r'^instructor/mentor/$', views.mentor, name='mentor'),
    url(r'^instructor/mentor/approval/$', views.mentor_approval, name='mentor_approval'),
	#Ogrenci Urlleri
    url(r'^student/$', views.student, name='student'),
    url(r'^student/exam_results/$', views.exam_results, name='exam_results'),
    url(r'^student/transcript/$', views.transcript, name='transcript'),
    url(r'^student/course_pick/$', views.course_pick, name='course_pick'),
    #Cikis Urlleri
	url(r'^[*]/cikis$', RedirectView.as_view(url='/cikis/')),
    url(r'^cikis/',views.logout_view),
]
